* Hauoli Mobile Tracking -- Demo Project on Unity Android

Requirement:
- armeabi-v7a Android device
- Android API Level 19
- Support 44100Hz or higher audio recording sampling rate

To get better accuracy:
- Keep the device stationary during calibration (3~5 seconds)
- Face the microphone to speakers
- Don't move too fast

SDK:
- hauoli-tracking.aar is expired on Nov. 25, 2017.
  Please contact us for the most updated SDK:
  http://www.hauoli.biz
  hauoli.info@gmail.com