﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using UnityEngine;

public class PointerControl : MonoBehaviour
{
    private int frame = 0;
    private int touchCnt = 0;

    // Screen Configuration / Coordinate Mapping
    private const int maxDim = 3;
    private float[][] RANGES_RCV;
    private float[][] RANGES_DISP;
    private float[] METER_TO_PIXEL_SCALE;

    // Speaker Configuration
    private double[] SPK_POS;
    private double[] INIT_POS;


    // Hauoli Tracker
    private AndroidJavaClass mTracker;
    private float[] mCoor = new float[maxDim];  // current coordinate

    private bool startTracking = false;         // if ready to track
    private bool threadRunning;                 // working in background
    
    
    void Start () {
        // Device Configuration
        configSpeakers();   // set up speaker and initial coordinate
        configScreen();     // mapping real to virtual coordinate
        
        
        // Initial Hauoli Tracker
        mTracker = new AndroidJavaClass("com.hauoli.tracking.HauoliTracker");
        mTracker.CallStatic<bool>("initTracker", SPK_POS, INIT_POS);
        mTracker.CallStatic<bool>("initInternalAudio");

        // Run tracking routine in background
        threadRunning = true;
        StartCoroutine(TrackingJob());
    }

    IEnumerator TrackingJob()
    {
        while(threadRunning) {
            if(startTracking) {
                int cnt = 1;
                while(true && threadRunning) {
                    if(cnt % 30 == 0) {
                        cnt = 0;
                        yield return null;
                    }
                    cnt ++;


                    // Do tracking here:
                    mTracker.CallStatic<double>("updateCoordinate");
                    mCoor[0] = (float) mTracker.CallStatic<double> ("getX");
                    mCoor[1] = (float) mTracker.CallStatic<double> ("getY");
                    mCoor[2] = (float) mTracker.CallStatic<double> ("getZ");
                    Debug.Log("x="+mCoor[0]+",y="+mCoor[1]+",z="+mCoor[2]);
                }
            }
            yield return null;
        }
    }


    /**
     * Close the APP
     **/
    void OnDisable()
    {
        // stop tracking job
        threadRunning = false;
        startTracking = false;
    }


    /**
     * Called once per frame
     **/
    void Update () 
    {
        Debug.Log("DEBUG: update Touch #"+touchCnt);
        // Capture touch events
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
            touchCnt ++;
            Debug.Log("Touch #"+touchCnt);

            // Start tracking when we touch the screen
            if(touchCnt >= 1) {
                if(!startTracking) {
                    mTracker.CallStatic<bool>("start");
                    Debug.Log("start tracking");
                    startTracking = true;    
                }
            }
        }

        // Move the pointer on the screen
        if(touchCnt >= 1) {
            Debug.Log("DEBUG: update touch Cnt >= 1, #"+touchCnt);

            if(mTracker.CallStatic<int>("getTrackingState") >= 2) {
                Debug.Log("DEBUG: cali done, #"+touchCnt);

                float[] ret = convertCoor(mCoor);
                transform.SetPositionAndRotation(
                    new Vector3(ret[0],ret[1],ret[2]), 
                    new Quaternion(0,0,0,0));
                Debug.Log(ret[0]+","+ret[1]+","+ret[2]);
            }
            else {
                transform.SetPositionAndRotation(
                    new Vector3(-10,0,0), 
                    new Quaternion(0,0,0,0));
            }
            frame ++;
        }
    }

    /**
     * Set up moving range in reality and corresponding range on screen
     **/
    void configScreen() {
        // Real coordinate range (in mm)
        RANGES_RCV = new float[maxDim][];
        // X-axis
        RANGES_RCV[0] = new float[2];
        RANGES_RCV[0][0] = 220f;
        RANGES_RCV[0][1] = 0f;
        // Y-axis
        RANGES_RCV[1] = new float[2];
        RANGES_RCV[1][0] = 0f;
        RANGES_RCV[1][1] = -300f;
        // Z-axis
        RANGES_RCV[2] = new float[2];
        RANGES_RCV[2][0] = 100f;
        RANGES_RCV[2][1] = 0f;
        

        // Virtual coordinate range on mobile device
        RANGES_DISP = new float[maxDim][];
        // X-axis
        RANGES_DISP[0] = new float[2];
        RANGES_DISP[0][0] = -3f;
        RANGES_DISP[0][1] = 3f;
        // Y-axis
        RANGES_DISP[1] = new float[2];
        RANGES_DISP[1][0] = -5f;
        RANGES_DISP[1][1] = 5f;
        // Z-axis
        RANGES_DISP[2] = new float[2];
        RANGES_DISP[2][0] = -3f;
        RANGES_DISP[2][1] = 3f;
        

        // Convertion between real and virtual coordinates
        METER_TO_PIXEL_SCALE = new float[maxDim];
        for(int i = 0; i < maxDim; i ++) {
            METER_TO_PIXEL_SCALE[i] = (float)((RANGES_DISP[i][1]-RANGES_DISP[i][0]) / (RANGES_RCV[i][1]-RANGES_RCV[i][0]));
        }
    }

    /**
     * Set up the speakers coordinates and initial position of phone
     **/
    void configSpeakers() {
        int nSpk = 2;
        int nDim = 2;
        
        // Speaker coordinates (in mm)
        //   e.g. left: (0,0), right: (220,0)
        SPK_POS = new double[nSpk * nDim];
        SPK_POS [0] = 0;
        SPK_POS [1] = 0;
        SPK_POS [2] = 220;
        SPK_POS [3] = 0;


        // Initial coordinates of mobile phone (in mm)
        //   e.g., (110, -100)
        INIT_POS = new double[nDim];
        INIT_POS [0] = 110;
        INIT_POS [1] = -100;
    }

    /**
     * Converting from real coordinate to virtual coordinate
     * @INPUT coor: real coordinate
     * @OUTPUT    : virtual coordinate
     **/
    float[] convertCoor(float[] coor)
    {
        float[] ret = new float[maxDim];
        for (int di = 0; di < maxDim; di++) {
            ret[di] = (coor[di]-RANGES_RCV[di][0]) * METER_TO_PIXEL_SCALE[di] + RANGES_DISP[di][0];
        }

        return ret;
    }
}
